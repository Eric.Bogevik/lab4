package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

int cols;
int rows;
CellState[][] grid;
    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        this.grid = new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= numRows()){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column >= numColumns()){
            throw new IndexOutOfBoundsException();
        }
        else{
            grid[row][column] = element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newgrid = new CellGrid(rows, cols, null);
        for (int x = 0; x < numRows(); x++) {
            for (int y = 0; y < numColumns(); y++) {
                newgrid.set(x, y, this.get(x,y));
            }
        }
        return newgrid;
    }
    
}
